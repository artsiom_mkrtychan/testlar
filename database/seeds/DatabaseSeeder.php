<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('janres')->insert([
            ['name' => 'инди', 'alias' => 'indy'],
            ['name' => 'рэп', 'alias' => 'rap'],
            ['name' => 'хип-хоп', 'alias' => 'hip_hop'],
            ['name' => 'поп', 'alias' => 'pop'],
            ['name' => 'альтернатива', 'alias' => 'alternative'],
            ['name' => 'блюз', 'alias' => 'blues']
        ]);

        DB::table('singers')->insert([
            [
                'name' => 'Ed sheeran',
                'alias' => 'ed_sheeran',
                'photo_url' => 'https://avatars.yandex.net/get-music-content/34131/188c69b0.p.381067/s200x200',
                'biography' => 'Британский автор-исполнитель, музыкант и актёр. Первый коммерческий успех пришёл к нему в июне 2011 года благодаря синглу «The A Team», занявшему третье место в британском чарте. Оба его альбома, + и x, занимали первые места в британском чарте, последний также возглавлял американский чарт. Песня Ширана «Thinking Out Loud» принесла ему две премии «Грэмми» в 2016 году. В январе 2017 сингл Shape of You занял 1-е место по продажам в США.'
            ],
            [
                'name' => 'Rag and bone man',
                'alias' => 'rag_n_bone_man',
                'photo_url' => 'https://avatars.yandex.net/get-music-content/42108/0a67cd98.p.1113157/s200x200',
                'biography' => 'Редакторы NME называют английского музыканта Рори Грэма «блюзменом XXI века». В 2016 году его сингл «Human» оказался на вершинах хит-парадов нескольких стран. А в 2017 году британские критики присудили ему награду «Brit Awards». «Я беспокоюсь, что люди, которым понравился мой сингл, думают, что весь альбом будет в таком духе. А это не так. Надеюсь, они будут приятно удивлены», — говорит Грэм.'
            ],
            [
                'name' => 'Birdy',
                'alias' => 'birdy',
                'photo_url' => 'https://avatars.yandex.net/get-music-content/63210/7a86e675.p.428727/s200x200',
                'biography' => 'Жасмин ван ден Богард, известная под псевдонимом Birdy, — британская певица, победительница конкурса Open Mic UK 2008 года. '
            ],
            [
                'name' => 'Broods',
                'alias' => 'brooks',
                'photo_url' => 'http://1720296432.rsc.cdn77.org/img/artists/578_artid/57828/broods-510881.jpg',
                'biography' => 'Broods — талантливейший дуэт из Окленда, Новая Зеландия. Он состоит из вокалистки Джорджии Нотт и ее старшего брата Калеба Нотта.'
            ]
        ]);

        DB::table('songs')->insert([
            [
                'name' => 'Shape of you',
                'alias' => 'shape_of_you',
                'words' => "
The club isn't the best place to find a lover
So the bar is where I go
Me and my friends at the table doing shots
Drinking faster and then we talk slow
Come over and start up a conversation with just me
And trust me I'll give it a chance now
Take my hand, stop
Put Van The Man 1 on the jukebox
And then we start to dance
And now I'm singing like

Girl, you know I want your love
Your love was handmade for somebody like me
Come on now, follow my lead
I may be crazy, don't mind me
Say, boy, let's not talk too much
Grab on my waist and put that body on me
Come on now, follow my lead
Come, come on now, follow my lead

I'm in love with the shape of you
We push and pull like a magnet do
Although my heart is falling too
I'm in love with your body
And last night you were in my room
And now my bedsheets smell like you
Every day discovering something brand new
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Every day discovering something brand new
I'm in love with the shape of you

One week in we let the story begin
We're going out on our first date
You and me are thrifty
So go all you can eat
Fill up your bag and I fill up a plate
We talk for hours and hours about the sweet and the sour
And how your family is doing okay
Leave and get in a taxi, then kiss in the backseat
Tell the driver make the radio play
And I'm singing like

Girl, you know I want your love
Your love was handmade for somebody like me
Come on now, follow my lead
I may be crazy, don't mind me
Say, boy, let's not talk too much
Grab on my waist and put that body on me
Come on now, follow my lead
Come, come on now, follow my lead

I'm in love with the shape of you
We push and pull like a magnet do
Although my heart is falling too
I'm in love with your body
And last night you were in my room
And now my bedsheets smell like you
Every day discovering something brand new
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Oh—i—oh—i—oh—i—oh—i
I'm in love with your body
Every day discovering something brand new
I'm in love with the shape of you

Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on
Come on, be my baby, come on

I'm in love with the shape of you
We push and pull like a magnet do
Although my heart is falling too
I'm in love with your body
Last night you were in my room
And now my bedsheets smell like you
Every day discovering something brand new
I'm in love with your body
Come on, be my baby, come on
Come on, be my baby, come on
I'm in love with your body
Come on, be my baby, come on
Come on, be my baby, come on
I'm in love with your body
Come on, be my baby, come on
Come on, be my baby, come on
I'm in love with your body
Every day discovering something brand new
I'm in love with the shape of you"
            ],
            [
                'name' => 'Eraser',
                'alias' => 'eraser',
                'words' => "
I was born inside a small town, I’ve lost that state of mind
Learned to sing inside the Lord’s house, but stopped at the age of nine
I forget when I get awards now the wave I had to ride
The paving stones I played upon, they kept me on the grind
So blame it on the pain that blessed me with the life
Friends and family filled with envy when they should be filled with pride
And when the world’s against me is when I really come alive
And everyday that Satan tempts me, I try to take it in my stride
You know that I’ve got whisky with white lines and smoke in my lungs
I think life has got to the point I know without it’s no fun
I need to get in the right mind and clear myself up Instead, I look in the mirror questioning what I’ve become
I guess it’s a stereotypical day for someone like me Without a nine-to-five job or an uni degree
To be caught up in the trappings of the industry
They showed me the locked doors I find another use for the key
And you’ll see
I’m well aware of certain things that will destroy a man like me But with that said give me one more, higher
Another one to take the sting away
I am happy on my own, so here I’ll stay
Save your lovin' arms for a rainy day
And I’ll find comfort in my pain
Eraser
I used to think that nothing could be better than touring the world with my songs
I chased the pictured perfect life, I think they
painted it wrong
I think that money is the root of evil and fame is hell
Relationships and hearts you fixed, they break as well
And ain’t nobody wanna see you down in the dumps
Because you’re living your dream, man, this shit should be fun
Please know that I’m not trying to preach like I’m Reverend Run
I beg you, don’t be disappointed with the man I’ve become
Our conversations with my father on the A14
Age twelve telling me I’ve gotta chase those dreams
Now I’m playing for the people, dad, and they know me With my 
beat and small guitar wearing the same old jeans
Wembley Stadium crowd two hundred and forty thou
I may have grown up but I hope that Damien’s proud
And to the next generation, inspiration’s allowed
The world may be filled with hate but keep erasing it now
Somehow
I’m well aware of certain things that will befall a man like me 
But with that said give me one more, higher
Another one to take the sting away
I am happy on my own, so here I’ll stay
Save your lovin arms for a rainy day
And I’ll find comfort in my pain
Eraser
And I’ll find comfort in my pain
Eraser
And I’ll find comfort in my pain
Eraser
I woke up this morning lookin' in the mirror
Thinkin' to myself that I should probably be thinner
The industry told me to look like them
But I found my happiness in fried food for my dinner
I wish that she could have been my first time
And I wish that I’d never took that first line
And I wish that every word in this verse rhymed
But forgive me if it doesn’t
I wish that I could make peace with my older cousin
I wish he didn’t think that it was me when it wasn’t
I wish I didn’t love it when I’m high and my face feels buzzin'
And the taste stays underneath my tongue
Wish that I had known what to do as a youngling
Wish I hadn’t dropped out of school and missed every single party
But that hardly matters now, man, does it?
Wish I had an answer to everything, but fuck it I wish creatin' art didn’t come with a budget
But while we’re on the subject
I wish my private life would have never gone public
But that’s the sacrifice that we make
Spendin' my whole time high livin' life away
Singin' this is how we’re livin' down here
Sittin' on the edge, lookin' out without fear
Yeah, we got drama but you know we don’t care
I wanna see you sing it, put your hands in the air, one wish
I’m singin' this is how we’re livin' down here
Sittin' on the edge, lookin' out without fear
Yeah, we got drama but you know we don’t care
I wanna see you sing it, put your hands in the air, one wish
I wish my family and friends they stay healthy
I wish that love was a currency and the whole world was wealthy
I found myself late night wishin' on a star
Everyday I wish I’d never broken a heart, uh And I wish I’d never run to 
Every woman that I loved that kept my life and what it’s come to 
I wish I was the role model you look up to If I told my fans 
the things I did they’d say, «Fuck you»
I wish I was home more
I wish my team could see their kids on the birthdays, but yo, we’re on tour
And I wish I’d grow more, wish I told more
People that I love 'em but it’s in the music that I’m known for
And I wish he never got cancer
And if I smoke a pack a day, well, does that make me a wanker?
Oh yes, I guess it does, and we’re still stressin' 'cause
Every day this shit gets the best of us Usin' my balance on a razor blade
Spendin' my whole time high wishing life would wait
Singin' this is how we’re livin' down here
Sittin' on the edge, lookin' out without fear
Yeah, we got drama but you know we don’t care
I wanna see you sing it, put your hands in the air, one wish
I’m singin' this is how we’re livin' down here
Sittin' on the edge, lookin' out without fear
Yeah, we got drama but you know we don’t care
I wanna see you sing it, put your hands in the air, one wish
I’m using jumpers for goalposts, cigarettes for throat cold
Mum sayin', «Don't smoke,» no, I don’t listen, I got
Love for a ghost note, shows on the gold coast
People that I don’t know share the same vision
I find truth in the hard times and words that aren’t mine
Tryin' to find a love with a compatible star sign
Sometimes I can’t write, sentences can’t rhyme
Starin' at my notepad quick, I’m tryin' to find mine
Shit, quick before I hit it again
Surrounded in the industry by all these ignorant men
And who knew that I’d be paid just to pick up a pen
Just let me hit the studio when we can rip it again
I’m a competitive dick, with an adrenaline kick
My daddy told me work hard and you can never be shit
I’ve seen all my heroes dethroned except my dad
Sat back here reminiscing 'bout the times we had
One wish"
            ],
            [
                'name' => 'Dive',
                'alias' => 'dive',
                'words' => "
Maybe I came on too strong
Maybe I waited too long
Maybe I played my cards wrong
Oh just a little bit wrong
Baby I apologise for it I could fall or I could fly
Here in your aeroplane
I could live, I could die
Hanging on the words you say
I’ve been known to give my all
And jumping in harder than
10,000 rocks on the lake
So don’t call me baby
Unless you mean it Don’t tell me you need me If you don’t 
believe it So let me know the truth
Before I dive right into you
You’re a mystery
I have travelled the world
And there’s no other girl like you, no one
What’s your history?
Do you have a tendency to lead some people on?
Cause I heard you do I could fall or I could fly
Here in your aeroplane
I could live, I could die
Hanging on the words you say
I’ve been known to give my all
And lie awake, every day
Don’t know how much I can take
So don’t call me baby
Unless you mean it Don’t tell me you need me If you don’t 
believe it So let me know the truth
Before I dive right into you
I could fall or I could fly
Here in your aeroplane
I could live, I could die
Hanging on the words you say
I’ve been known to give my all
Sitting back, looking at Every mess that I made
So don’t call me baby
Unless you mean it Don’t tell me you need me If you don’t 
believe it So let me know the truth
Before I dive right into you
Before I dive right into you
Before I dive right into you"
            ],


            [
                'name' => 'Human',
                'alias' => 'human',
                'words' => "
Maybe I'm foolish, maybe I'm blind
Thinking I can see through this and see what's behind
Got no way to prove it so maybe I'm blind

But I'm only human after all, I'm only human after all
Don't put your blame on me

Take a look in the mirror and what do you see
Do you see it clearer or are you deceived in what you believe

Cos I'm only human after all, you're only human after all
Don't put the blame on me
Don't put your blame on me

Some people got the real problems
Some people out of luck
Some people think I can solve them
Lord heavens above
I'm only human after all, I'm only human after all
Don't put the blame on me
Don't put the blame on me

Don't ask my opinion, don't ask me to lie
Then beg for forgiveness for making you cry, making you cry

Cos I'm only human after all, I'm only human after all
Don't put your blame on me, don't put the blame on me

Some people got the real problems
Some people out of luck
Some people think I can solve them
Lord heavens above
I'm only human after all, I'm only human after all
Don't put the blame on me
Don't put the blame on me
I'm only human I make mistakes
I'm only human that's all it takes to put the blame on me
Don't put your blame on me

Cos I'm no prophet or messiah
Should go looking somewhere higher

I'm only human after all, I'm only human after all
Don't put the blame on me, don't put the blame on me
I'm only human I do what I can
I'm just a man, I do what I can
Don't put the blame on me
Don't put your blame on me"
            ],
            [
                'name' => 'Skin',
                'alias' => 'skin',
                'words' => "
When I heard that sound
When the walls came down
I was thinking about you
About you
When my skin grows old
When my breath runs cold
I’ll be thinking about you
About you
Seconds from my heart
A bullet from the dark
Helpless, I surrender
Shackled by your love
Holding me like this
With poison on your lips
Only when it’s over
The silence hits so hard
'Cause it was almost love, it was almost love
It was almost love, it was almost love
When I heard that sound
When the walls came down
I was thinking about you
About you
When my skin grows old
When my breath runs cold
I’ll be thinking about you
About you
When I run out of air to breathe
It’s your ghost I see
I’ll be thinking about you, about you
It was almost love, it was almost…
We bleed ourselves in vain
How tragic is this game?
Turn around, I’m holding on to someone
But the love is gone
Carrying the load, with wings that feel like stone
Knowing that we nearly fell so far now
It’s hard to tell
Yeah we came so close, it was almost love
It was almost love, it was almost love
When I heard that sound
When the walls came down
I was thinking about you
About you
When my skin grows old
When my breath runs cold
I’ll be thinking about you
About you
When I run out of air to breathe
It’s your ghost I see
I’ll be thinking about you, about you
While I reached out for your hand
When the walls were caving in When I see you on the other side
We can try all over again
When I heard that sound
When the walls came down
I was thinking about you
About you
When my skin grows old
When my breath runs cold
I’ll be thinking about you
About you
When I run out of air to breathe
It’s your ghost I see
I’ll be thinking about you, about you
'Cause it was almost love, it was almost love
It was almost love, it was almost love"
            ],
            [
                'name' => 'Ego',
                'alias' => 'ego',
                'words' => "
Runnin' wild, round a roundabout
Got it figured out, why you only go in circles
You can lie, all your walls around
All them building blocks
No exit, no exit
Baby I know my gospel but I ain’t a preacher
Sorry to burst your bubble, but somebody needs to We’re-a feed your ego
Ego
Bang bang baby, down you fall
Ain’t you mister know it all?
Ego
Ego
Bang bang baby, down you fall
Ain’t you mister know it all?
Sing it loud just like you believe
How to receive
Everything you said was perfect
Livin' life so delusional
You could lose it all
No question, no question
Baby I know my gospel but I ain’t a preacher
Sorry to burst your bubble but somebody needs to We’re-a feed your ego
Ego
Bang bang baby, down you fall
Ain’t you mister know it all?
Ego
Ego
Bang bang baby, down you fall
Ain’t you mister know it all?
I bet you stopped to see the car crash didn’t you?
You told yourself that it’s a rotten world didn’t you?
Personified the greatness and reap the rewards
Convinced the words are something real 'til they’re strikin' a chord
You musta been
Pushing your way so your headpiece is so big
You so rotten my brother, what a way to live
Top of the food chain, trucking the loose chains
But who are they to say you got no taste
You got no hold bars, mister know it all
A noble star, he don’t know that he won’t go far
Cuz he ain’t no fool, but he’s tucked up at night
With a belly full of «I'm so cold», and that’s life
Baby I know my gospel but I ain’t a preacher (I ain’t a preacher)
Sorry to burst your bubble but somebody needs to We’re-a feed your ego
Ego (Something's got to give)
Bang bang baby, down you fall
Ain’t you mister know it all?
Ego
Ego
Bang bang baby, down you fall
Ain’t you mister know it all?
Something’s got to give
Something’s got to give
Something’s got to give
Something’s got to give"
            ],


            [
                'name' => 'Shelter',
                'alias' => 'shelter',
                'words' => "
I find shelter, in this way
Under cover, hide away
Can you hear, when I say?
I have never felt this way
Maybe I had said, something that was wrong
Can I make it better, with the lights turned on 
Maybe I had said, something that was wrong
Can I make it better, with the lights turned on 
Could I be, was I there?
It felt so crystal in the air
I still want to drown, whenever you leave
Please teach me gently, how to breathe
And I’ll cross oceans, like never before
So you can feel the way I feel it too
And I’ll mirror images back at you
So you can see the way I feel it too
Maybe I had said, something that was wrong
Can I make it better, with the lights turned on 
Maybe I had said, something that was wrong
Can I make it better, with the lights turned on 
Maybe I had said, something that was wrong
Can I make it better, with the lights turned on"
            ],
            [
                'name' => 'Wings',
                'alias' => 'wings',
                'words' => "
Sunlight comes creeping in Iluminates our skin
We watch the day go by Stories of what we did
It made me think of you
It made me think of you
Under a trillion stars
We danced on top the cars
Took pictures of the state
So far from where we are"
            ],
            [
                'name' => 'Words',
                'alias' => 'words',
                'words' => "
Waiting on you
Trying to keep your head strong
With nothing to lose
You raise your voice with something to prove
And all the things you say to me
I can’t forget them
You don’t leave
But you tell me with your eyes what you need
Oh, please
Do you think that I don’t know what it means?
All the things you hide for me I accept them
But I need you next to me If I can’t hold you now
Keep thinking that you might not come around
I have no words, I have no words to say
If I can’t change your mind
Keep thinking, is this our last goodbye?
You say it first, you say it first to me You’re in the clear
While I’m waking up to nothing but tears
And you say they’ve said
That I’m the only one that needed to change
You know the things you’ve said to me Do you regret them?
I just need you next to me If I can’t hold you now
Keep thinking that you might not come around
I have no words, I have no words to say
If I can’t change your mind
Keep thinking, is this our last goodbye?
You say it first, you say it first to me So 
I can’t just forget you, just forget you
Can’t just forget you, just forget you
And If I can’t hold you now
Keep thinking how you might not come around
And I have no words, I have no words to say
If I can’t change your mind
Keep thinking, is this our last goodbye?
You say it first, you say it first to me If 
I can’t hold you now (if I can’t hold you now)
Keep thinking that you might not come around (might not come around)
I have no words, I have no words to say
If I can’t change your mind (if I can’t change your mind)
Keep thinking, is this our last goodbye? (our last goodbye?)
You say it first, you say it first to me"
            ],


            [
                'name' => 'Bridges',
                'alias' => 'bridges',
                'words' => "
Gave you a minute
When you needed an hour
Chose to push it aside
Instead of leaving behind you
If any word that I said
Could have made you forget
I’d have given you them all
But it was all in your head
Now we’re burning all the bridges now
Watching it go up in flames
No way to build it up again
And we’re burning all the bridges now
'Cause it was sink or swim
And I went down, down, down
And we’re burning all the bridges
Burning all the bridges now
And we’re burning all the bridges
Burning all the bridges now
If I didn’t hit it Would you still say you 
needed me Guess I walked right into it Guess I made it too easy
If any word that you said
Could have made me forget
Would I get up off the floor
'Cause this is all in my head
And we’re burning all the bridges now
Watching it go up in flames
No way to build it up again
And we’re burning all the bridges
Burning all the bridges now
And we’re burning all the bridges
Burning all the bridges now
Can we forget about it Can we forget
Can we forget about it Can we forget
Burning all the bridges
Burning all the bridges now
And we’re burning all the bridges
Burning all the bridges now"
            ],
            [
                'name' => 'Everytime',
                'alias' => 'everytime',
                'words' => "
Are you with us, darling?
Cause you treat it like a game
And you mess yourself up It’s such a shame, such a shame
You got issues, darling
Cause you wasted all away
You’re full of yourself
It’s all in vain, all in vain
And it breaks my heart
And it breaks my heart
Oh, Oh Cause every time is the last time (Oh, Oh)
And I’m kickin' myself just trying to be understanding (Oh, Oh)
Tell me how can you fake that? (Oh, oh)
You’re leaving me waitin' and acting like I’m so demanding (Oh, Oh)
Cause it’s never your fault
When you’re keepin' your knees clean
And «sorry\"'s below you
Its always me, always me If it breaks your heart
When you break my heart…
Oh, Oh Cause every time is the last time (Oh, Oh)
And I’m kickin' myself just trying to be understanding (Oh, Oh)
Tell me how can you fake that? (Oh, oh)
You’re leaving me waitin' and acting like I’m so demanding (Oh, Oh)
You told me you could change your ways
You collect in strides but you look away
You promised me you could make it better
You told me you won’t be the same
But your eyes stay shut when I scream, fall faint
I only wanted to make it better
Make it better
Make you better
And it breaks my heart
And it breaks my heart
Oh, Oh Cause every time is the last time (Oh, Oh)
And I’m kickin' myself just trying to be understanding (Oh, Oh)
Tell me how can you fake that? (Oh, oh)
You’re leaving me waitin' and acting like I’m so demanding (Oh, Oh)
Oh, Oh Cause every time is the last time (Oh, Oh)
And I’m kickin' myself just trying to be understanding (Oh, Oh)
Tell me how can you fake that? (Oh, oh)
You’re leaving me waitin' and acting like I’m so demanding (Oh, Oh)"
            ],
            [
                'name' => 'Free',
                'alias' => 'free',
                'words' => "
I’d lose everything so I can sing.
Hallelujah I’m free.
I’m free.
I’m free.
I’m free.
I’m free.
Ahh, ahh, ahh, ahh.
I have lived my life so perfectly.
Kept to all my lines so carefully.
I’d lose everything so I can sing.
Hallelujah I’m free.
I’m free.
I’m free.
I’m free.
I’m free.
Ahh.
Ahh.
Ahh.
Hallelujah I’m free.
Gritting your teeth, you hold onto me.
It’s never enough, I’m never complete.
Tell me to prove, expect me to lose.
I push it away, I’m trying to move.
Hoping for more, and wishing for less.
When I didn’t care was when I did best.
I’m desperate to run, I’m desperate to leave.
If I lose it all, at least I’ll be free.
Ahh.
Ahh.
All I want is your attention please.
Don’t want your opinion or your fee.
'cause the freest I have ever been.
I had nothing to show or be seen.
I’m free.
I’m free.
I’m free.
I’m free.
Gritting your teeth, you hold onto me.
It’s never enough, I’m never complete.
Tell me to prove, expect me to lose.
I push it away, I’m trying to move.
Hoping for more, and wishing for less.
When I didn’t care was when I did best.
I’m desperate to run, I’m desperate to leave.
If I lose it all, at least I’ll be free.
Gritting your teeth, you hold onto me.
It’s never enough, I’m never complete.
Tell me to prove, expect me to lose.
I push it away, I’m trying to move.
Hoping for more, and wishing for less.
When I didn’t care was when I did best.
I’m desperate to run, I’m desperate to leave.
If I lose it all, at least I’ll be free.
It’s clear you think that I’m inferior.
Whatever helps you sleep at night.
Whatever helps you keep it tight.
It’s clear you think that I’m inferior.
Whatever helps you sleep at night.
Whatever helps you keep it tight.
Gritting your teeth, you hold onto me.
It’s never enough, I’m never complete.
Tell me to prove, expect me to lose.
I push it away, I’m trying to move.
Hoping for more, and wishing for less.
When I didn’t care was when I did best.
I’m desperate to run, I’m desperate to leave.
If I lose it all, at least I’ll be free.
Gritting your teeth, you hold onto me.
It’s never enough, I’m never complete.
Tell me to prove, expect me to lose.
I push it away, I’m trying to move.
Hoping for more, and wishing for less.
When I didn’t care was when I did best.
I’m desperate to run, I’m desperate to leave.
If I lose it all, at least I’ll be free.
Ahh.
Ahh.
Ahh.
Hallelujah I’m free."
            ],
        ]);

        DB::table('singer_janres')->insert([
            ['singer_id' => 1, 'janre_id' => 4],
            ['singer_id' => 1, 'janre_id' => 2],
            ['singer_id' => 1, 'janre_id' => 3],

            ['singer_id' => 2, 'janre_id' => 2],
            ['singer_id' => 2, 'janre_id' => 3],
            ['singer_id' => 2, 'janre_id' => 6],

            ['singer_id' => 3, 'janre_id' => 1],
            ['singer_id' => 3, 'janre_id' => 4],

            ['singer_id' => 4, 'janre_id' => 1],
            ['singer_id' => 4, 'janre_id' => 4],
            ['singer_id' => 4, 'janre_id' => 5]
        ]);

        DB::table('singer_songs')->insert([
            ['singer_id' => 1, 'song_id' => 1],
            ['singer_id' => 1, 'song_id' => 2],
            ['singer_id' => 1, 'song_id' => 3],

            ['singer_id' => 2, 'song_id' => 4],
            ['singer_id' => 2, 'song_id' => 5],
            ['singer_id' => 2, 'song_id' => 6],

            ['singer_id' => 3, 'song_id' => 7],
            ['singer_id' => 3, 'song_id' => 8],
            ['singer_id' => 3, 'song_id' => 9],

            ['singer_id' => 4, 'song_id' => 10],
            ['singer_id' => 4, 'song_id' => 11],
            ['singer_id' => 4, 'song_id' => 12]
        ]);
    }
}

