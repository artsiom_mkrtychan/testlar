<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SingerSong extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'singer_songs';
}
