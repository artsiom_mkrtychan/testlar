<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Janre extends Model
{
    public function singers()
    {
        return $this->belongsToMany('App\Singer', 'singer_janres', 'janre_id', 'singer_id');
    }
}
