<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Singer extends Model
{
    public function songs()
    {
        return $this->belongsToMany('App\Song', 'singer_songs', 'singer_id', 'song_id');
    }

    public function janres()
    {
        return $this->belongsToMany('App\Janre', 'singer_janres', 'singer_id', 'janre_id');
    }
}
