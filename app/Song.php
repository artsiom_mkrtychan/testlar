<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    public function singers()
    {
        return $this->belongsToMany('App\Singer', 'singer_songs', 'song_id', 'singer_id');
    }


}
