<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Janre;
use App\Singer;
use App\SingerJanre;
use App\SingerSong;
use App\Song;
use App\Utils;

class SingerController extends Controller
{
    public function show($singerAlias)
    {
        $singer = Singer::where('alias',$singerAlias)->first();       
		$songs = $singer->songs()->paginate(2);

        return view(
            'singer',
            [
                'singer'=> $singer,
                'songs'=> $songs
            ]
        );
    }
}
