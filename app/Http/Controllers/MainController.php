<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Janre;
use App\Singer;
use App\SingerJanre;
use App\SingerSong;

class MainController extends Controller
{
    public function show()
    {
        $paginateSingers = Singer::paginate(2);

        return view(
            'singers',
            [
                'title'=> 'Singers',
                'singers'=> $paginateSingers,
            ]
        );
    }
}
