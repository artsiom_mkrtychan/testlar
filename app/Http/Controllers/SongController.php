<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Janre;
use App\Singer;
use App\SingerJanre;
use App\SingerSong;
use App\Song;

class SongController extends Controller
{
    public function show($singerAlias, $songAlias)
    {
        $song = Song::where('alias', $songAlias)->first();
        $singer = Singer::where('alias', $singerAlias)->first();

        return view(
            'song',
            [
                'singer' => $singer,
                'song' => $song
            ]
        );
    }
}
