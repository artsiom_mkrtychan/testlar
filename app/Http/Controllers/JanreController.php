<?php

namespace App\Http\Controllers;

use App\Janre;
use App\Utils;

class JanreController extends Controller
{
    public function show($janreAlias)
    {
        $janre = Janre::where('alias', $janreAlias)->first();        
        $paginateSingers = $janre->singers()->paginate(2);
		 
        return view(
            'singers',
            [
                'title' => 'Janres: ' . $janre->name,
                'singers' => $paginateSingers
            ]
        );
    }
}
