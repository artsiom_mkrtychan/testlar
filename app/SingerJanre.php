<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SingerJanre extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'singer_janres';
}
