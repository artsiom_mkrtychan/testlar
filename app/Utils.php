<?php
/**
 * Created by PhpStorm.
 * User: grantemon
 * Date: 28.03.2017
 * Time: 19:47
 */

namespace App;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class Utils
{
    static function paginate($items, $perPage)
    {
        if (is_array($items)) {
            $items = collect($items);
        }

        return new LengthAwarePaginator(
            $items->forPage(Paginator::resolveCurrentPage(), $perPage),
            $items->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );
    }
}