@extends('main')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h2>{{$title}}</h2>
                </div>
                <div class="col-md-2">
                    <h3>
                        <a href="/">to main</a>
                    </h3>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="singers-wrap">
                @foreach ($singers as $singer)
                    <div class="singer-wrap">
                        <div class="row ">
                            <div class="vertical-align">
                                <div class="col-md-1">
                                    <img src="{{ $singer->photo_url }}" class="img-rounded singer-icon">
                                </div>
                                <div class="col-md-5">
                                    <h3><a href="/singers/{{ $singer->alias }}">{{ $singer->name }}</a></h3>
                                </div>
                            </div>
                        </div>
                        <div>
                            <span>Janres: </span>
                            @foreach ($singer->janres as $janre)
                                <a href="/janres/{{ $janre->alias }}">#{{ $janre->name }}</a>
                            @endforeach
                        </div>
                        <div>
                            <span>Songs: </span>
                            {{$singer->songs->count()}}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                {{ $singers->links() }}
            </div>
        </div>
    </div>
@endsection
