@extends('main')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="singer-header">{{ $singer->name }}</div>
        </div>
        <div class="col-md-2">
            <div class="back-ref"><a href="/">to main</a></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <img src="{{ $singer->photo_url }}" class="img-rounded singer-icon-lg">
        </div>
        <div class="col-md-4">
            <div>
                {{ $singer->biography }}
            </div>
        </div>
    </div>
    <div>
        <span>Janres: </span>
        @foreach ($singer->janres as $janre)
            <a href="/janres/{{ $janre->alias }}">#{{ $janre->name }}</a>
        @endforeach
    </div>
    <h2>Songs:</h2>
    <div class="songs-wrap">
        @foreach ($songs as $song)
            <div class="row ">
                <div class="col-md-5">
                    <h3><a href="{{ $singer->alias }}/{{ $song->alias }}">{{ $song->name }}</a></h3>
                </div>
            </div>
        @endforeach
    </div>
    <div class="text-center">
        {{ $songs->links() }}
    </div>
@endsection