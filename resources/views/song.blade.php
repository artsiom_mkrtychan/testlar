@extends('main')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="singer-header">
                <div><a href="/singers/{{ $singer->alias }}">{{ $singer->name }}</a></div>
                <div>{{$song->name}}</div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="back-ref"><a href="/">back</a></div>
        </div>
    </div>
    <div>
        <span>Janres: </span>
        @foreach ($singer->janres as $janre)
            <a href="/janres/{{ $janre->alias }}">#{{ $janre->name }}</a>
        @endforeach
    </div>

    <br/>
    <div class="row">
        <div class="col-md-6">
            <pre>
                {{$song->words}}
            </pre>
        </div>
    </div>
@endsection
