<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Janre;
use App\Singer;
use App\Song;
use App\SingerSong;
use App\SingerJanre;

Route::get('/', 'MainController@show');
Route::get('/singers/{singerAlias}', 'SingerController@show');
Route::get('/singers/{singerAlias}/{songAlias}', 'SongController@show');
Route::get('/janres/{janreAlias}', 'JanreController@show');
Route::get('/test', function (){
    echo Singer::find(1)->janres;
});